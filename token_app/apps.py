from django.apps import AppConfig

from web3 import Web3
from web3.middleware import geth_poa_middleware

from core.settings import WEB3_INFURA_PROJECT_ID, ADDRESS_CONTRACT, ABI


class TokenAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'token_app'
    verbose_name = 'Токены'

    def ready(self):
        self.ready_contract()

    def ready_contract(self):
        self.w3 = Web3(Web3.HTTPProvider(WEB3_INFURA_PROJECT_ID))

        # variables from .env file
        address_contract = ADDRESS_CONTRACT
        abi = ABI

        # прослойка между сетями
        self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)

        assert True is self.w3.is_connected()

        # smart-contract
        self.contract_instance = self.w3.eth.contract(address=address_contract, abi=abi)
