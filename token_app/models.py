from django.db import models


class Token(models.Model):
    unique_hash = models.CharField('Уникальный хеш', max_length=20, unique=True)
    tx_hash = models.CharField('Хеш транзакции', max_length=127, unique=True)
    media_url = models.URLField('Ссылка на медиа')
    owner = models.CharField('Владелец', max_length=50)

    class Meta:
        verbose_name = ("Токен")
        verbose_name_plural = ("Токены")
