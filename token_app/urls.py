from django.urls import path

from .views import TokenViewSet


urlpatterns = [
    path('create/', TokenViewSet.as_view({'post': 'create'})),
    path('list/', TokenViewSet.as_view({'get': 'list'})),
    path('total_supply/', TokenViewSet.as_view({'get': 'total_supply'})),
]
