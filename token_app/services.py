from typing import Any

import uuid

from django.apps import apps

from core.settings import SIGN_TRANSACTION_ADDRESS, SIGN_TRANSACTION_ADDRESS_SECRET_MNEMONIC, CHAIND_ID


class TokenService:
    def __init__(self, **kwargs: Any) -> None:
        config = apps.get_app_config('token_app')
        self._contract_instance = config.contract_instance
        self._w3 = config.w3
        super().__init__(**kwargs)

    def total_supply(self):
        return self._contract_instance.functions.totalSupply().call()

    def create_token(self, owner, media_url):
        self.unique_hash = uuid.uuid4().hex[-20:]

        encrypted_key = SIGN_TRANSACTION_ADDRESS_SECRET_MNEMONIC

        self._w3.eth.account.enable_unaudited_hdwallet_features()

        # create private_key
        account = self._w3.eth.account.from_mnemonic(encrypted_key)

        # nonce
        nonce = self._w3.eth.get_transaction_count(SIGN_TRANSACTION_ADDRESS)

        # mint
        unicorn_txn = self._contract_instance.functions.mint(owner, self.unique_hash, media_url).build_transaction({
            'chainId': CHAIND_ID, 'from': SIGN_TRANSACTION_ADDRESS, 'gas': '0',
            'nonce': nonce, 'gasPrice': int(self._w3.eth.gas_price * 1.101)})

        gas = self._w3.eth.estimate_gas(unicorn_txn)
        unicorn_txn.update({'gas': gas})

        # create signature
        signed = self._w3.eth.account.sign_transaction(unicorn_txn, account._private_key)

        # execute transaction and get answer
        self.tx_hash = self._w3.eth.send_raw_transaction(signed.rawTransaction)

        return self.unique_hash, self.tx_hash
