from typing import Any
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from .serializers import TokenSerializer
from .services import TokenService


class TokenViewSet(viewsets.ModelViewSet):
    serializer_class = TokenSerializer
    model = serializer_class.Meta.model
    queryset = model.objects.all()

    def __init__(self, **kwargs: Any) -> None:
        self.token_service = TokenService()
        super().__init__(**kwargs)

    def create(self, request, *args, **kwargs):
        # business (nft) logic
        owner = request.data.get('owner')
        media_url = request.data.get('media_url')
        unique_hash, tx_hash = self.token_service.create_token(owner, media_url)

        # api logic
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(unique_hash=unique_hash, tx_hash=tx_hash.hex())
        headers = self.get_success_headers(serializer.data)

        # reponse
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=False)
    def total_supply(self, request):
        return Response({'result': self.token_service.total_supply()})
