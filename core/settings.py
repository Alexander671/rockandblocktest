import os
from pathlib import Path

import environ

CORE_DIR = Path(__file__).resolve().parent
BASE_DIR = CORE_DIR.parent

env = environ.Env()
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))
PROJECT_NAME = env.str('PROJECT_NAME')
ENV = env.str('ENV')
VERSION = env.str('VERSION')

DEBUG = env.bool('DEBUG')
SECRET_KEY = env.str('SECRET_KEY')
ALLOWED_HOSTS = env.list('URLS')

WEB3_INFURA_PROJECT_ID = env.str('WEB3_INFURA_PROJECT_ID')
ADDRESS_CONTRACT = env.str('ADDRESS_CONTRACT')
SIGN_TRANSACTION_ADDRESS = env.str('SIGN_TRANSACTION_ADDRESS')
SIGN_TRANSACTION_ADDRESS_SECRET_MNEMONIC = env.str('SIGN_TRANSACTION_ADDRESS_SECRET_MNEMONIC')

ABI_FILE_PATH = env.str('ABI_FILE_PATH')
with open(ABI_FILE_PATH, 'r') as file:
    ABI = file.read()

PAGINATION_PAGE_SIZE = env.int('PAGINATION_PAGE_SIZE')
CHAIND_ID = env.int('CHAIN_ID')


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # inner apps
    'token_app',
    # outer apps
    'rest_framework',
    'drf_spectacular',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'core.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'core.wsgi.application'


# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env.str('POSTGRES_DB'),
        'USER': env.str('POSTGRES_USER'),
        'PASSWORD': env.str('POSTGRES_PASSWORD'),
        'HOST': env.str('POSTGRES_HOST'),
        'PORT': env.str('POSTGRES_PORT'),
    },
}


# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# Default primary key field type
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# REST
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.AllowAny',
    ],

    # Для swagger'a
    'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',

    # Пагинация
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': PAGINATION_PAGE_SIZE
}

# SWAGGER
SPECTACULAR_SETTINGS = {
    'TITLE': 'RockAndBlockTest API',
    'DESCRIPTION': 'Endpoints for RockAndBlockTest API',
    'VERSION': 'v1',
    'SERVE_INCLUDE_SCHEMA': False,
    'LICENSE': {'name': 'BSD License'},
    'COMPONENT_SPLIT_REQUEST': True,

    'SERVE_PUBLIC': True,
    'SERVE_PERMISSIONS': ['rest_framework.permissions.AllowAny'],
}
